/**
  ******************************************************************************
  * @file    FMLR.h
  * @author  Alex Raimondi
  * @version V1.0.0
  * @date    17-Dec-2016
  * @brief   This file contains definitions for:
  *          - LEDs available on FMLR
  *          - Flash device on FMLR
  ******************************************************************************
  */

/** @addtogroup BSP
  * @{
  */

/** @addtogroup MIROMICO_FMLR
  * @{
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __FMLR_H
#define __FMLR_H

#ifdef __cplusplus
 extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32l1xx_hal.h"


/** @defgroup MIROMICO_FMLR_Exported_Types Exported Types
  * @{
  */
typedef enum
{
  LED_RED,
  LED_GREEN
} Led_TypeDef;

/**
  * @}
  */

/** @defgroup MIROMICO_FMLR_Exported_Constants Exported Constants
  * @{
  */

/**
  * @brief  Define for MIROMICO_FMLR board
  */
#if !defined (USE_MIROMICO_FMLR)
 #define USE_MIROMICO_FMLR
#endif

/** @defgroup MIROMICO_FMLR_LED LED Constants
  * @{
  */
#define LEDn                             2

#define LEDG_PIN                         GPIO_PIN_2
#define LEDG_GPIO_PORT                   GPIOB
#define LEDG_GPIO_CLK_ENABLE()           __HAL_RCC_GPIOB_CLK_ENABLE()
#define LEDG_GPIO_CLK_DISABLE()          __HAL_RCC_GPIOB_CLK_DISABLE()

#define LEDR_PIN                         GPIO_PIN_0
#define LEDR_GPIO_PORT                   GPIOB
#define LEDR_GPIO_CLK_ENABLE()           __HAL_RCC_GPIOB_CLK_ENABLE()
#define LEDR_GPIO_CLK_DISABLE()          __HAL_RCC_GPIOB_CLK_DISABLE()

#define LEDx_GPIO_CLK_ENABLE(__INDEX__)  do { LEDR_GPIO_CLK_ENABLE();} while(0)

#define LEDx_GPIO_CLK_DISABLE(__INDEX__) (LEDR_GPIO_CLK_DISABLE())

/**
  * @}
  */

/** @addtogroup MIROMICO_FMLR_Exported_Functions
  * @{
  */
uint32_t        BSP_GetVersion(void);

/** @addtogroup MIROMICO_FMLR_LED_Functions
  * @{
  */

void            BSP_LED_Init(Led_TypeDef Led);
void            BSP_LED_On(Led_TypeDef Led);
void            BSP_LED_Off(Led_TypeDef Led);
void            BSP_LED_Toggle(Led_TypeDef Led);

/**
  * @}
  */

/**
  * @}
  */

#ifdef __cplusplus
}
#endif

#endif /* __FMLR_H */

/**
  * @}
  */

/**
  * @}
  */

